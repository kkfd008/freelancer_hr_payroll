// JavaScript Document
/*!
 * selectOptionM v1.4
 * robin.hu 2012.11.28
 * 模拟下拉框样式
 .selectOption{}
 .chooseBox{width:134px; height:30px; float:left;line-height:30px;background:url(../images/chooseBg.gif) no-repeat; margin-top:8px;_margin-left:3px;}
 .choosen{width:100%; height:30px;text-indent:5px;}
 .chooseBox div.optionBox{border:1px solid #C5C5C5;background:#fff;}
 .chooseBox a{color:#474747;background:#fff; clear:both; display:block; height:30px;line-height:30px; text-indent:5px;cursor:pointer;overflow:hidden;}
 .chooseBox a:hover{ color:#fff;background:#78ACCE;}
 .chooseBox a.choosenLink{color:#fff;background:#75989A;}
 * 调用方法
 * $(".chooseBox select").selectOptionM({mouseleave:true,position:"top",maxHeight:200});
 */
(function ($) 
{
    $.fn.selectOptionM = function (options)
    {
        return this.each(function (i) 
        {
            var node = this.nodeName.toLowerCase();
            if (node == 'select') {
                new selectOptionM(this, options);
            }
        });
    };
    selectOptionM = function (el, options) 
    {
        var obj = this;
        var settings = $.extend({}, $.selectOptionM.defaults, options || {});
        obj.el = el;
        $(obj.el).hide();
        //原下拉框隐藏
        obj.opp = $(obj.el).parent(".chooseBox");
        obj.ops = $(obj.el).find("option");
        obj.width = obj.opp.width();
        obj.height = obj.opp.height();
        if (obj.ops.length == 0) {
            return false;
        };
		if (obj.opp.find(".choosen").length == 0) {
        obj.opp.append('<span class="choosen"></span><div class="optionBox"></div>');
		}
        obj.mychoosen = obj.opp.find(".choosen");
        obj.myoptionBox = obj.opp.find(".optionBox");
		
		//组件必须样式定义
		obj.opp.css("position", "relative");
		obj.mychoosen.attr("style", " display:inline-block;cursor:pointer;");
		obj.myoptionBox.attr("style", "display:none; overflow:auto;position:absolute;");
		obj.myoptionBox.width(obj.width - 2);
		//组件必须样式定义

		$.extend(obj, {
            choosenOption : function (aLink)
			{//执行选择操作，对select填值
				var choosenOp = $(aLink);
				var chooseId = choosenOp.val() || choosenOp.attr("linkVal");
				$(obj.el).val(chooseId).change();
				obj.mychoosen.text(choosenOp.text());
				obj.mychoosen.attr("title",choosenOp.text());
				$(obj.myoptionBox).find("a[linkVal=" + chooseId + "]").addClass("choosenLink").siblings().removeClass("choosenLink");
			},
			init: function()
			{//初始化
				obj.myoptionBox.html("");
				for (i = 0; i < obj.ops.length; i++)
				{
					obj.myoptionBox.append('<a linkVal="' + $(obj.ops[i]).val() + '" title="'+$(obj.ops[i]).text()+'" href="####">' + $(obj.ops[i]).text() + '</a>');
				}
				obj.selected = $(obj.el).find("option:selected");
				if (obj.selected.length > 0) {
					obj.choosenOption($(obj.selected));
				} else {
					obj.choosenOption($(obj.ops[0]));
				}
				if ($(obj.myoptionBox).height() > settings.maxHeight) {
					$(obj.myoptionBox).height(settings.maxHeight);
				}
				//$(obj.mychoosen).unbind('click').bind('click',function ()
				$(obj.mychoosen).unbind('click').bind('click',function ()
				{ 
					obj.showOption();
				});
				obj.aLinks = obj.myoptionBox.find("a");
				obj.aLinks.click(function ()
				{
					obj.choosenOption($(this));
					obj.hideOption();
				});
				if (settings.mouseleave) {
					$(obj.opp).mouseleave(function ()
					{
						obj.hideOption();
					});
				}
				//设定位置
				obj.pos = $(obj.opp).offset();//下拉菜单选择区域位置
				obj.windowHeight = $(settings.myContent).height();//外容器高度
				obj.totalHeight = $(obj.myoptionBox).height();//下拉区域高度
				obj.bHeight = obj.windowHeight - (obj.pos.top + obj.totalHeight);
				if (obj.bHeight < 0) {
					if(obj.totalHeight > obj.pos.top){
						obj.setPosition("bottom");
					}else{
						obj.setPosition("top");
					}
				}else{
					obj.setPosition("bottom");
				}
			},
			setPosition:function (position)
			{//设定myoptionBox的位置
				var myoptionBox = obj.myoptionBox;
				var toppos = 0;
				var leftpos = 0;
				switch (position) 
				{
					case "bottom":
						toppos = obj.height - 1 ;
						break;
					case "top":
						toppos = - 1 * obj.totalHeight - 1;
						break;
					default:
						toppos = obj.height - 1 ;
						break;
				}
				obj.myoptionBox.css({
					'top' : toppos + 'px', 'left' : leftpos + 'px' 
				});
			},
			showOption : function ()
			{
				if ($(obj.myoptionBox).is(":visible")) {
					obj.hideOption();
					return false;
				}
				$(".optionBox").hide();
				$(obj.opp).css("z-index", 100);
				$(obj.opp).parent().css("z-index", 100);
				obj.myoptionBox.fadeIn("fast");
			},
			hideOption : function ()
			{
				obj.myoptionBox.fadeOut("fast");
				$(obj.opp).css("z-index", 90);
				$(obj.opp).parent().css("z-index", 0);
			}
		});
        obj.init();//初始化创建下拉菜单
		
    }
    $.selectOptionM = 
    {//默认配置属性
        defaults : 
        {
			myContent: $("body"),//默认外容器
            mouseleave : true, //默认鼠标离开下拉菜单消失
            maxHeight : 400, //下拉菜单最大高度400px
            position : "bottom"//下拉菜单显示在选择框底部
        }
    }
})(jQuery);