// JavaScript Document
(function($) {//隔行换色
	$.fn.changeTrBg = function(){
	    return this.each(function (i) {
            var node = this.nodeName.toLowerCase();
            if (node == 'table') {
                new changeTrBg(this);
            }
        });
	};
	changeTrBg = function (el) {
		var obj = this;
		obj.el = el;
		obj.trA = $(obj.el).find("tr");
		for(i=0;i<obj.trA.length;i++){
			if(i%2){$(obj.trA[i]).addClass("color");}
		}
	}
})(jQuery);

//Jquery serializeArray()方法扩充，将FORM内容拼成json字符串
//用法：var formJson = $("#"+formId).serializeObject();
(function($) {
	$.fn.serializeObject = function(strSplit)
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	    	if(strSplit!=null){
	    		var nameArr = this.name.split(strSplit);
	    		if (nameArr.length>1) {
		    		this.name = nameArr[1];
				}
	    	}
	    	if ($.trim(this.value)!='') {
	    		if (o[this.name]) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || '');
		        } else {
           			o[this.name] = this.value || '';
		        }
			}
	    });
	    return o;
	};
})(jQuery);

//用户选择绑定事件
$("#userChange").live("change",function(){
	var v = $(this).val();
	//alert(v);
	//执行搜索事件
});

$(document).ready(function(){
	$(".freelancerBody table").changeTrBg();
	$(".chooseBox select").selectOptionM();//创建模拟下拉菜单
});
