<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
final class sp_response
{
	private $template;
	private $output;
	private $redirect = NULL;
	
	public function __get($property)
	{
		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . ' no exist $property');
		return $this->$property;
	}
	
	public function __set($property, $value)
	{
// 		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . ' no exist $property');
// 		$this->$property = $value;
		if ('redirect' == $property && is_string($value)) {
			$this->redirect = $value;
		}
		return $this;
	}
	
	public function __construct()
	{
		$this->template = new sp_response_template();
		$this->output = new sp_response_output();
	}
}

?>