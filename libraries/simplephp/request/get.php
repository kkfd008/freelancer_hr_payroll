<?php
if (!defined('FRAMEWORK_SIMPLEPHP_VERSION')) exit('framework not setting,cannot load'.__FILE__);
class sp_request_get extends sp_input
{
	public function __construct()
	{
		$_data = @$_GET;
		if (is_array ( $_data ) && 0 < count ( $_data ))
		{
			foreach ( $_data as $_key => $_value )
			{
				$this->$_key = $_value;
			}
		}
	}
}