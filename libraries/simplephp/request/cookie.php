<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
class sp_request_cookie extends sp_input
{
	public function __construct()
	{
		
		$this->_initProperty();
	}
	
	private function _initProperty()
	{
		$_data = @$_COOKIE;
		if ( is_array( $_data ) && 0 < count( $_data ) )
		{
			foreach ( $_data as $_key => $_value )
			{
				$this->$_key = $_value;
			}
		}
	}
	
	public function set($property, $value)
	{
		if ( is_string( $value ) || is_numeric( $value ) ) $_COOKIE [$property] = $value;
		return $this;
	}
}