<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
class sp_request_session extends sp_input
{
	private $id = null;
	public function __construct()
	{
		
		$this->_initProperty();
	}
	
	private function _initProperty()
	{
		$_data = @$_SESSION;
		if ( is_array( $_data ) && 0 < count( $_data ) )
		{
			foreach ( $_data as $_key => $_value )
			{
				$this->$_key = $_value;
			}
		}
	}
	
	public function set($property, $value)
	{
		if ( is_string( $value ) || is_numeric( $value ) || is_serialized_string( $value ) ) $_SESSION [$property] = $value;
		return $this;
	}
	
	public function start()
	{
		if ( headers_sent( $file, $line ) )
		{
			echo "session waring \r\n $file \r\nthe $line line already output session cannt start";
			die();
		}
		
		if ( ! isset( $_SESSION ) )
		{
			session_start();
		}
		
		$this->_initProperty();
		return $this;
	}
	
	public function getId()
	{
		return session_id();
	}
	
	public function setId($id = null)
	{
		if ( null !== $id ) session_id( $id );
		return $this;
	}
}