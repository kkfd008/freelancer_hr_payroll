<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
final class sp_dispatcher
{
	private $controller;
	private $request;
	private $response;
	
	function __get($property)
	{
		if ( property_exists( $this, $property ) )
		{
			return $this->$property;
		}
		else
		{
			throw new Exception( __CLASS__ . " no property $property exist" );
		}
	}
	
	public function __construct()
	{
		$this->request = new sp_request( );
		$this->response = new sp_response( );
	}
	
	private function _getbycmd($command)
	{
		if ('_' != substr($command, 0,1) || '_' == $command) return false;
		$_commandcount = sizeof(explode('_', 'controller_'.trim($_REQUEST['cmd'],'_')));
		if (2 >= $_commandcount) return false;
		$_subPoint = strripos( $command, '_' );
		$_mvc['controller'] = 'controller' . substr( $command, 0, $_subPoint );
		$_mvc['method'] = substr( $command, $_subPoint + 1 );
		return $_mvc;
	}
	
	private function _getbyrouter()
	{
		
	}
	
	private function _getmvc()
	{
		$_mvc = $_default = array('controller' => 'sp_index','method' => 'index');
		if ( array_key_exists( 'cmd', $_REQUEST ) ) $_mvc = $this->_getbycmd($_REQUEST['cmd']);
		if ( false != $_mvc && class_exists( $_mvc['controller'] ) && method_exists( $_mvc['controller'], $_mvc['method'] ) ) return $_mvc;
		return $_default;
	}
	
	public function run()
	{
		if ( $this->controller->response->redirect )
		{
			return $this->redirect( );
		}
		return $this->display( );
	}
	
	public function render()
	{
		$_mvc = $this->_getmvc( );
		$this->controller = new $_mvc['controller']( $this->request, $this->response );
		$this->controller->$_mvc['method']( );
		return $this;
	}
	
	public function redirect()
	{
		if ( headers_sent( $file, $line ) )
		{
			echo $file;
			echo $line;
			return true;
		}
		$_url = $this->controller->response->redirect;
		header( "Location: $_url" );
		return true;
	}
	
	public function display()
	{
		$this->controller->response->output->display( );
		return true;
	}
}