<?php
/**
 * 日志记录类
 * @package Log
 * @author liuwen
 */
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
if ( ! defined( 'PROJECT_LOG_ROOT' ) ) define( 'PROJECT_LOG_ROOT', '/tmp' );
class sp_log
{
	public static $domain = 'localhost';
	public static $dport = '80';
	public static $host = '127.0.0.1';
	public static $port = '80';
	
	static public function init()
	{
		$servername = explode( ':', strtolower( $_SERVER["SERVER_NAME"] ) );
		if ( $servername[0] ) self::$domain = $servername[0];
		if ( 2 == sizeof( $servername ) ) self::$dport = $servername[1];
		self::$host = ($_SERVER["SERVER_ADDR"]) ? $_SERVER["SERVER_ADDR"] : '127.0.0.1';
		self::$port = ($_SERVER['SERVER_PORT']) ? $_SERVER["SERVER_PORT"] : '80';
	}
	
	static public function _getFile($type = 'info')
	{
		$basefile = join( '_', array(
			$type,
			self::$domain,
			self::$dport,
			self::$host,
			self::$port
		) );
		$logs = self::_getLog( );
		$logPath = PROJECT_LOG_ROOT . $logs['path'] . '/' . $type;
		
		if ( ! is_writable( $logPath ) )
		{
			self::_createFolders( $logPath );
		}
		$logFile = basename( $basefile ) . '_' . $logs['filename'];
		
		return $logPath . '/' . $logFile;
	}
	
	static public function writeline($msg, $type = 'info', $prefix = TRUE)
	{
		$msg = str_replace( '|', '', $msg ) . "\r\n";
		self::write( $msg, $type, $prefix );
		return TRUE;
	}
	
	static public function write($msg, $type = 'info', $prefix = TRUE)
	{
		self::init( );
		$file = self::_getFile( $type );
		if ( $prefix ) $msg = mktime( time( ) ) . "|" . str_replace( '|', '', $msg );
		error_log( $msg, 3, $file );
		return TRUE;
	}
	
	static private function _createFolders($path)
	{
		// 递归创建,如果文件夹不存在
		if ( ! file_exists( $path ) )
		{
			self::_createFolders( dirname( $path ) ); // 取得最后一个文件夹的全路径返回开始的地方
			mkdir( $path, 0777 );
		}
	}
	
	static private function _getLog($today = NULL)
	{
		if ( NULL == $today )
		{
			$today = explode( '|', date( 'Y|m|d|H|i|s', mktime( ) ) );
		}
		$logs['path'] = '/' . $today[0] . '/' . $today[0] . $today[1] . $today[2];
		$logs['filename'] = $today[0] . $today[1] . $today[2] . $today[3];
		return $logs;
	}
}
