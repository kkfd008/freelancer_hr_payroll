<?php
/**
 * FILTER_CALLBACK：调用用户自定义函数来过滤数据。
 * FILTER_SANITIZE_FULL_SPECIAL_CHARS
 * FILTER_SANITIZE_STRING：去除标签，去除或编码特殊字符。
 * FILTER_SANITIZE_STRIPPED："string" 过滤器的别名。
 * FILTER_SANITIZE_ENCODED：URL-encode 字符串，去除或编码特殊字符。
 * FILTER_SANITIZE_SPECIAL_CHARS：HTML 转义字符 '"<>& 以及 ASCII 值小于 32 的字符。
 * FILTER_SANITIZE_EMAIL：删除所有字符，除了字母、数字以及 !#$%&'*+-/=?^_`{|}~@.[]
 * FILTER_SANITIZE_URL：删除所有字符，除了字母、数字以及 $-_.+!*'(),{}|\\^~[]`<>#%";/?:@&=
 * FILTER_SANITIZE_NUMBER_INT：删除所有字符，除了数字和 +-
 * FILTER_SANITIZE_NUMBER_FLOAT：删除所有字符，除了数字、+- 以及 .,eE。
 * FILTER_SANITIZE_MAGIC_QUOTES：应用 addslashes()。
 * FILTER_UNSAFE_RAW：不进行任何过滤，去除或编码特殊字符。
 * FILTER_VALIDATE_INT：在指定的范围以整数验证值。
 * FILTER_VALIDATE_BOOLEAN：如果是 "1", "true", "on" 以及 "yes"，则返回 true，如果是 "0", "false", "off", "no" 以及 ""，则返回 false。否则返回 NULL。
 * FILTER_VALIDATE_FLOAT：以浮点数验证值。
 * FILTER_VALIDATE_REGEXP：根据 regexp，兼容 Perl 的正则表达式来验证值。
 * FILTER_VALIDATE_URL：把值作为 URL 来验证。
 * FILTER_VALIDATE_EMAIL：把值作为 e-mail 来验证。
 * FILTER_VALIDATE_IP：把值作为 IP 地址来验证。 
 * @author qinqin
 *
 */

/**
 * FILTER_FLAG_STRIP_LOW	FILTER_SANITIZE_ENCODED, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_STRING, FILTER_UNSAFE_RAW	 Strips characters that has a numerical value <32.
 * FILTER_FLAG_STRIP_HIGH	FILTER_SANITIZE_ENCODED, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_STRING, FILTER_UNSAFE_RAW	 Strips characters that has a numerical value >127.
 * FILTER_FLAG_ALLOW_FRACTION	FILTER_SANITIZE_NUMBER_FLOAT	 Allows a period (.) as a fractional separator in numbers.
 * FILTER_FLAG_ALLOW_THOUSAND	FILTER_SANITIZE_NUMBER_FLOAT, FILTER_VALIDATE_FLOAT	 Allows a comma (,) as a thousands separator in numbers.
 * FILTER_FLAG_ALLOW_SCIENTIFIC	FILTER_SANITIZE_NUMBER_FLOAT	 Allows an e or E for scientific notation in numbers.
 * FILTER_FLAG_NO_ENCODE_QUOTES	FILTER_SANITIZE_STRING	 If this flag is present, single (') and double (") quotes will not be encoded.
 * FILTER_FLAG_ENCODE_LOW	FILTER_SANITIZE_ENCODED, FILTER_SANITIZE_STRING, FILTER_SANITIZE_RAW	 Encodes all characters with a numerical value <32.
 * FILTER_FLAG_ENCODE_HIGH	FILTER_SANITIZE_ENCODED, FILTER_SANITIZE_SPECIAL_CHARS, FILTER_SANITIZE_STRING, FILTER_SANITIZE_RAW	 Encodes all characters with a numerical value >127.
 * FILTER_FLAG_ENCODE_AMP	FILTER_SANITIZE_STRING, FILTER_SANITIZE_RAW	 Encodes ampersands (&).
 * FILTER_NULL_ON_FAILURE	FILTER_VALIDATE_BOOLEAN	 Returns NULL for unrecognized boolean values.
 * FILTER_FLAG_ALLOW_OCTAL	FILTER_VALIDATE_INT	 Regards inputs starting with a zero (0) as octal numbers. This only allows the succeeding digits to be 0-7.
 * FILTER_FLAG_ALLOW_HEX	FILTER_VALIDATE_INT	 Regards inputs starting with 0x or 0X as hexadecimal numbers. This only allows succeeding characters to be a-fA-F0-9.
 * FILTER_FLAG_IPV4	FILTER_VALIDATE_IP	 Allows the IP address to be in IPv4 format.
 * FILTER_FLAG_IPV6	FILTER_VALIDATE_IP	 Allows the IP address to be in IPv6 format.
 * FILTER_FLAG_NO_PRIV_RANGE	FILTER_VALIDATE_IP	
 * \\Fails validation for the following private IPv4 ranges: 10.0.0.0/8, 172.16.0.0/12 and 192.168.0.0/16.
 * \\Fails validation for the IPv6 addresses starting with FD or FC.
 * FILTER_FLAG_NO_RES_RANGE	FILTER_VALIDATE_IP	 Fails validation for the following reserved IPv4 ranges: 0.0.0.0/8, 169.254.0.0/16, 192.0.2.0/24 and 224.0.0.0/4. This flag does not apply to IPv6 addresses.
 * FILTER_FLAG_PATH_REQUIRED	FILTER_VALIDATE_URL	 Requires the URL to contain a path part.
 * FILTER_FLAG_QUERY_REQUIRED	FILTER_VALIDATE_URL	 Requires the URL to contain a query string.
 * @author qinqin
 *
 */

/**
 * filter_has_var — Checks if variable of specified type exists
 * filter_id — Returns the filter ID belonging to a named filter
 * filter_input_array — Gets external variables and optionally filters them
 * filter_input — Gets a specific external variable by name and optionally filters it
 * filter_list — Returns a list of all supported filters
 * filter_var_array — Gets multiple variables and optionally filters them
 * filter_var — Filters a variable with a specified filter
 * @author qinqin
 *
 */
class sp_filter
{
	
}