<?php
if (!defined('FRAMEWORK_SIMPLEPHP_VERSION')) exit('framework not setting,cannot load'.__FILE__);
class sp_exception extends Exception
{
	// protected $message ;
	// protected $code ;
	// protected $file ;
	// protected $line ;
	
	public function __construct($message = "", $code = 0, $previous = NULL)
	{
		//my exception construct code
		parent::__construct($message,$code,$previous);
	}
}