<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
class sp_loader
{
	private $perfixPath = array (
		
		'sp_' => 'libraries_simplephp_', 
		'extends_' => 'libraries_extends_', 
		'plugin_' => 'libraries_plugin_', 
		'model_' => 'application_module_', 
		'controller_' => 'application_controller_', 
		'helper_' => 'application_helper_' 
	);
	
	public function setPerfixPath($perfix, $path)
	{
		if ( file_exists( PROJECT_DOCUMENT_ROOT . '/' . str_replace( '_', '/', $path ) ) && is_string( $perfix ) )
		{
			$this->perfixPath [$perfix] = $path;
		}
		else
		{
			return false;
		}
	}
	
	public function registerAutoload()
	{
		return spl_autoload_register( array (
			
			$this, 
			'includeClass' 
		) );
	}
	
	public function unregisterAutoload()
	{
		return spl_autoload_unregister( array (
			
			$this, 
			'includeClass' 
		) );
	}
	
	public function includeClass($class)
	{
		$_class = strtolower( trim( $class ) );
		$_perfix = substr( $_class, 0, strpos( $_class, '_' ) + 1 );
		if ( array_key_exists( $_perfix, $this->perfixPath ) )
		{
			$_class = str_replace( $_perfix, $this->perfixPath [$_perfix], $_class );
		
		}
		
// 		foreach ( $this->perfixPath as $_key => $_value )
// 		{
// 			if ( $_class != $_key && 0 === strpos( $_class, $_key ) )
// 			{
// 				$_class = str_replace( $_key, $_value, $_class );
// 				break;
// 			}
// 		}
		
		$_classfile = PROJECT_DOCUMENT_ROOT . '/' . trim( str_replace( '_', '/', $_class ) ) . '.php';
		// var_dump($_classfile);
		if ( file_exists( $_classfile ) )
		{
			require_once ($_classfile);
			// plog loading success
		}
		else
		{
			// plog loading failer
		}
		return true;
	}
}

?>