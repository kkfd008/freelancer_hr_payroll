<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_MONITOR' ) ) define( 'FRAMEWORK_SIMPLEPHP_MONITOR', FALSE );
final class sp_output
{
	private $header = array ();
	private $body = "welcome join simple php world";
	private $monitor = "";
	
	public function __construct()
	{
		
	}
	
	public function __get($property)
	{
		if ( property_exists( $this, $property ) )
		{
			return $this->$property;
		}
		else
		{
			throw new Exception( 'property :' . $property . ' not exist' );
		}
	}
	
	public function __set($property, $value)
	{
		if ( property_exists( $this, $property ) )
		{
			$this->$property = $value;
		}
		else
		{
			throw new Exception( 'property :' . $property . ' not exist' );
		}
	}
	
	public function display()
	{
		//run header;
		echo $this->body;
		if ( FALSE !== FRAMEWORK_SIMPLEPHP_MONITOR )
		{
			echo $this->_getmonitor();
		}
	}
	
	private function _getmonitor()
	{
		$_info = "\r\n<fieldset><legend align='center'><u>simplephp monitoring panel</u></legend>";
		$_info .= $this->monitor;
		foreach ( $_GET as $_key => $_value )
		{
			$_info .= '[GET][' . $_key . ']=' . $_value . '<br/>';
		}
		$_info .= "<br/>";
		foreach ( $_POST as $_key => $_value )
		{
			$_info .= '[POST][' . $_key . ']=' . $_value . '<br/>';
		}
		$_info .= '[PAGE][time]:' . date( 'Y-m-d H:i:s' );
		return $_info . '</fieldset>';
	}
}