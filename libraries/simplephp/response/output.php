<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_MONITOR' ) ) define( 'FRAMEWORK_SIMPLEPHP_MONITOR', FALSE );
class sp_response_output
{
	private $header = array ();
	private $body = "welcome join simple php world";
	private $monitor = "";
	
	public function __get($property)
	{
		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . ' no exist $property');
		return $this->$property;
		
		
	}
	
 	public function __set($property, $value)
	{
		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . ' no exist $property');
		$this->$property = $value;
	}
	
	public function display()
	{
		//run header;
		echo $this->body;
		if ( FALSE !== FRAMEWORK_SIMPLEPHP_MONITOR )
		{
			echo $this->_getmonitor();
		}
	}
	
	private function _getmonitor()
	{
		$_info = "\r\n<fieldset><legend align='center'><u>simplephp monitoring panel</u></legend>";
		$_info .= $this->monitor;
		foreach ( $_GET as $_key => $_value )
		{
			$_info .= '[GET][' . $_key . ']=' . $_value . '<br/>';
		}
		$_info .= "<br/>";
		foreach ( $_POST as $_key => $_value )
		{
			$_info .= '[POST][' . $_key . ']=' . $_value . '<br/>';
		}
		$_info .= '[PAGE][time]:' . date( 'Y-m-d H:i:s' );
		return $_info . '</fieldset>';
	}
}