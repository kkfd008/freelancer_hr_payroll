<?php
if ( defined( 'PROJECT_DOCUMENT_ROOT' ) 
		&& defined( 'PROJECT_APPLICATION_CONFIG' ) 
		&& defined( 'PROJECT_LIBRARIES_SIMPLEPHP' )
		&& file_exists( $loadfile = PROJECT_LIBRARIES_SIMPLEPHP . '/loader.php' ) )
{
	define( 'FRAMEWORK_SIMPLEPHP_VERSION', '1.00.00' );
	require_once ($loadfile);
}
else
{
	die( 'application start must definde PROJECT_DOCUMENT_ROOT,PROJECT_APPLICATION_CONFIG,PROJECT_LIBRARIES_SIMPLEPHP' );
}

abstract class sp_bootstrap
{
	// property
	private $autoloader;
	private $dispatcher;
	
	public function __get($property)
	{
		if ( ! property_exists( $this, $property ) ) throw new sp_exception( __CLASS__ . "no exits $property" );
		return $this->$property;
	}
	
	public function __construct()
	{
		$this->autoloader = new sp_loader( );
		$this->autoloader->registerAutoload( );
		$this->dispatcher = new sp_dispatcher( );
	}
	
	public function run()
	{
		$this->dispatcher->render( )->run( );
		return true;
	}

}

interface sp_applicaiton
{
	public function run();
}

interface sp_cli
{
	public function run();
}