<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
if ( ! defined( 'FRAMEWORK_TEMPLATE_PATH' ) ) define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/template/default' );
abstract class sp_controller
{
	protected $request;
	protected $response;
	
	public function __get($property)
	{
		return $this->$property;
	}
	
	public function __construct(sp_request $request, sp_response $response)
	{
		$this->request = $request;
		$this->response = $response;
		$this->_init();
	}
	
	public function __destruct()
	{
		$this->_unset();
	}
	
	protected function _init()
	{
	
	}
	
	protected function _unset()
	{
		
	}
	
	protected function forward($controller, $method ,$request = null,$response = null)
	{
		if ( class_exists( $controller ) && method_exists( $controller, $method ) )
		{
			if ($request instanceof sp_request) {
				$this->request = $request;
			}
			if ($response instanceof sp_response) {
				 $this->response = $response;
			}
			$forward = new $controller($this->request,$this->response);
			$forward->$method();
		}
		return null;
	}
}
