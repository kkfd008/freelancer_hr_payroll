<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
final class sp_db
{
	public static $_instance;
	public static function &instance($database, $extclass = 'extends_db_mysql')
	{
		if ( FALSE == class_exists( $extclass ) ) return null;
		
		if ( ! self::$_instance[$extclass][$database] instanceof $extclass )
		{
			$_param = sp_environment::get("$extclass:$database");
			self::$_instance[$extclass][$database] = new $extclass( $database, $_param['host'], $_param['port'], $_param['username'], $_param['password'] );
		}
		return self::$_instance[$extclass][$database];
	}
}
?>