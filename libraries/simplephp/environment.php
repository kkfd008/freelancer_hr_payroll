<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
if ( ! defined( 'PROJECT_APPLICATION_CONFIG' ) ) exit( 'PROJECT_APPLICATION_CONFIG no define' );
class sp_environment
{
	private static $config = null;
	
	static public function init()
	{
		self::$config = array();
		if ( ! file_exists( PROJECT_APPLICATION_CONFIG ) ) return false;
		$_content = include PROJECT_APPLICATION_CONFIG;
		if ( is_array( $_content ) && 0 < sizeof( $_content ) )
		{
			self::$config = $_content;
		}
	}
	
	static public function get($path = null)
	{
		if ( ! is_array( self::$config ) ) self::init( );
		if ( null == $path ) return self::$config;
		$path = explode( ':', $path );
		$data = self::$config;
		foreach ( $path as $value )
		{
			if ( is_array( $data ) && array_key_exists( $value, $data ) )
			{
				$data = $data[$value];
			}
			else
			{
				$data = null;
			}
		
		}
		return $data;
	}
}

?>