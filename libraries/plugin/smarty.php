<?php
class plugin_smarty
{
	private static $instance = null;
	
	static public function &instance()
	{
		require_once (PROJECT_DOCUMENT_ROOT . '/libraries/plugin/smarty/Smarty.class.php');
		if ( ! self::$instance instanceof Smarty )
		{
// 			$config = sp_environment::get('' );
// 			$smarty_config = $config['plugin_template_smarty'];
			$smarty_config = sp_environment::get('plugin_template_smarty');
			self::$instance = new Smarty( );
			self::$instance->setTemplateDir( $smarty_config['template_dir'] );
			self::$instance->setCompileDir( $smarty_config['compile_dir'] );
			if ( '' != $smarty_config['config_dir'] ) self::$instance->setConfigDir( $smarty_config['config_dir'] );
			if ( '' != $smarty_config['cache_dir'] ) self::$instance->setCacheDir( $smarty_config['cache_dir'] );
			if ( '' != $smarty_config['plugins_dir'] ) self::$instance->setPluginsDir( $smarty_config['plugins_dir'] );
			if ( '' != $smarty_config['left_delimiter'] ) self::$instance->left_delimiter = $smarty_config['left_delimiter'];
			if ( '' != $smarty_config['right_delimiter'] ) self::$instance->right_delimiter = $smarty_config['right_delimiter'];
		}
		return self::$instance;
	}
	
	public function __destruct()
	{
	
	}
}
