<?php
abstract class extends_db_mysql_parse extends extends_db_parse
{
	// public function pagesize($size, $page = 1)
	// {
	// $this->offset(0);
	// if (is_int($page) && $page > 1) $this->offset(($page-1)*$size);
	// $this->limit($size);
	// }
	
	protected function parseWhere()
	{
		$parseWhere = ' WHERE';
		if ( 0 >= sizeof( $this->condition ) ) return null;
		$parseWhere .= ' '.join(' AND ', $this->condition);
		return $parseWhere;
	}
	
	protected function parseSet()
	{
		if ( 0 >= sizeof( $this->data ) ) return null;
		$parseSet = $set = '';
		foreach ( $this->data as $key => $value )
		{
			// $set = '`'.$key.'` = \''.$value .'';
			if ( is_numeric( $value ) ) $set .= " `$key` = $value";
			$parseSet .= ",`$key` = '$value'";
		}
		return ' SET ' . trim( $parseSet, ',' );
	}
	
	protected function parseOrder()
	{
		if ( 0 >= sizeof( $this->sort ) ) return null;
		foreach ( $this->sort as $key => $value )
		{
			// $set = '`'.$key.'` = \''.$value .'';
			$parseSet .= ",`$key` $value";
		}
		return ' ORDER BY ' . trim( $parseSet, ',' );
	}
	
	protected function parseLimit()
	{
		$parseLimit = '';
		if ( $this->offset && $this->limit )
		{
			$parseLimit = " LIMIT $this->offset,$this->limit";
		}
		elseif ( ! $this->offset && $this->limit )
		{
			$parseLimit = " LIMIT 0,$this->limit";
		}
		else
		{
			$parseLimit = " LIMIT 0,20";	
		}
		return $parseLimit;
	}
}

