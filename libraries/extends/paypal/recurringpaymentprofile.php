<?php
class extends_paypal_recurringpaymentprofile extends extends_paypal_nvp
{
	private $profileid;
	private $ack;
	private $startdate;
	private $period;
	private $frequency;
	
	public function setProfileid($profileid)
	{
		$this->profileid = $profileid;
	}
	
	public function create($token, $currenycode, $amt, $period, $frequency, $desc, $startdate = null, array $options = array())
	{
		if ( ! $token || 20 != strlen( $token ) ) return false;
		$querydata['METHOD'] = 'CreateRecurringPaymentsProfile';
		$querydata['VERSION'] = '51.0';
		$querydata['TOKEN'] = $token;
		$querydata['CURRENCYCODE'] = $currenycode;
		$querydata['AMT'] = $amt;
		$querydata['DESC'] = $desc;
		
		// billing period or "Day", "Week", "Month","SemiMonth","Year"
		// 循环周期
		$querydata['BILLINGPERIOD'] = $period;
		
		// billing frequency
		// 循环频率 1+ 跟billing period相关联
		$querydata['BILLINGFREQUENCY'] = $frequency;
		
		// profiles startdate
		// 循环支付开始时间
		if ( null == $startdate )
		{
			$querydata['PROFILESTARTDATE'] = date( 'Y-n-j').'T0:0:0';
		}
		else
		{
			$querydata['PROFILESTARTDATE'] = $startdate;
		}
		//$querydata['PROFILESTARTDATE'] = '2012-11-6T0:0:0';
		// opentions 其他选项
		
		$_result = $this->_curl( $querydata );
		if ( "SUCCESS" == strtoupper( $_result["ACK"] ) || "SUCCESSWITHWARNING" == strtoupper( $_result["ACK"] ) )
		{
			$this->ack = $_result['ACK'];
			$this->profileid = $_result['PROFILEID'];
			return $_result;
		}
		else
		{
			return false;
		}
	}
	
	// https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_GetRecurringPaymentsprofileidetails
	public function getDetails()
	{
		if ( ! $this->profileid ) return false;
		$querydata['METHOD'] = 'GetRecurringPaymentsProfileDetails';
		$querydata['VERSION'] = '92.0';
		$querydata['PROFILEID'] = $this->profileid;
		// METHOD
		$_result = $this->_curl( $querydata );
		var_dump($_result);
		if ( "SUCCESS" == strtoupper( $_result["ACK"] ) || "SUCCESSWITHWARNING" == strtoupper( $_result["ACK"] ) )
		{
			return $_result;
		}
		else
		{
			return false;
		}
	}
	
	// https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_ManageRecurringPaymentsProfileStatus
	public function manageStatus($action)
	{
		if ( ! $this->profileid ) return false;
		$querydata['METHOD'] = 'ManageRecurringPaymentsProfileStatus';
		$querydata['VERSION'] = '92.0';
		$querydata['PROFILEID'] = $this->profileid;
		/**
		 * Cancel – Only profiles in Active or Suspended state can be canceled.
		 * Suspend – Only profiles in Active state can be suspended.
		 * Reactivate – Only profiles in a suspended state can be reactivated.
		 */
		$querydata['ACTION'] = $action;
		// ACTION
	}
	
	public function Update()
	{
		if ( ! $this->profileid ) return false;
		$querydata['METHOD'] = 'UpdateRecurringPaymentsProfile';
		$querydata['VERSION'] = '92.0';
		$querydata['PROFILEID'] = $this->profileid;
	}
	
	public function billOutstandingAmount()
	{
		if ( ! $this->profileid ) return false;
		$querydata['METHOD'] = 'BillOutstandingAmount';
		$querydata['VERSION'] = '92.0';
		$querydata['PROFILEID'] = $this->profileid;
	}
}