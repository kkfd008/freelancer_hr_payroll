<?php
abstract class extends_paypal_nvp
{
	protected $environment = 'sandbox';
	protected $user = 'mtcard_1350380100_biz_api1.gmail.com';
	protected $pass = '1350380200';
	protected $signature = 'Ak0WKxuhJ1Jfrk4q6XbPjH6NuM8WAq-RCI-TKw45dgNZwKT625JQVWoA';
	
	protected function _curl(array $querydata, $mode = 'post')
	{
		// &METHOD=SetExpressCheckout&AMT=10.00
		$querydata['USER'] = $this->user;
		$querydata['PWD'] = $this->pass;
		$querydata['SIGNATURE'] = $this->signature;
	
		$_nvpreq = trim( http_build_query( $querydata ) );
		$ch = curl_init( );
		curl_setopt( $ch, CURLOPT_URL, $this->getEndPoint( ) );
		curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
	
		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
	
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		// Set the request as a POST FIELD for curl.
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $_nvpreq );
	
		// Get response from the server.
		$httpResponse = curl_exec( $ch );
		curl_close( $ch );
	
		if ( ! $httpResponse )
		{
			die( 'curl_init connection paypal webservice failer' );
		}
	
		$httpResponseAr = explode( "&", $httpResponse );
	
		$httpParsedResponseAr = array();
		foreach ( $httpResponseAr as $i => $value )
		{
			$tmpAr = explode( "=", $value );
			if ( sizeof( $tmpAr ) > 1 )
			{
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}
		return $httpParsedResponseAr;
	}
	
	public function getEndPoint()
	{
		if('live' == $this->environment) $_environment = NULL;
		$_environment = 'sandbox';
		return "https://api-3t.$_environment.paypal.com/nvp";
	}
	
	public function getTokenPoint($useraction = false)
	{
		if ( ! $this->token || 20 != strlen( $this->token ) ) return false;
		if('live' == $this->environment) $_environment = NULL;
		$_environment = 'sandbox';
		$useraction = (false == $useraction) ? NULL : '&useraction=commit';
		return "https://www.$_environment.paypal.com/webscr?cmd=_express-checkout$useraction&token=" . urlencode( $this->token );
	}
	
	public function connPayPal($user, $pass, $signature, $environment = null)
	{
		$this->environment = $environment;
		$this->user = $user;
		$this->pass = $pass;
		$this->signature = $signature;
	}
}