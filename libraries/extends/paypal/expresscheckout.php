<?php
class extends_paypal_expresscheckout extends extends_paypal_nvp
{
	private $checkoutstatus;
	private $payerstatus;
	private $payerid;
	private $payemail;
	private $countrycode;
	private $currenycode;
	private $amt;
	private $paymentaction;
	
	private $paymentstatus;
	private $paymenttype;
	
	// BILLINGAGREEMENTACCEPTEDSTATUS
	private $billingagreementacceptedstatus = 0;
	
	public function __get($property)
	{
		if ( property_exists( $this, $property ) )
		{
			return $this->$property;
		}
		else
		{
			throw new sp_exception_property( "property $property no exist" );
		}
	}
	
	public function setToken(array $querydata)
	{
		// Set request-specific fields.
		$querydata['METHOD'] = 'SetExpressCheckout';
		$querydata['VERSION'] = '92.0';
		
		// if ( ! isset( $querydata['RETURNURL'] ) ) $querydata['RETURNURL'] = '';
		// if ( ! isset( $querydata['CANCELURL'] ) ) $querydata['CANCELURL'] = '';
		// if ( ! isset( $querydata['AMT'] ) ) $querydata['AMT'] = '0.00';
		
		// Set the curl parameters.
		$_result = $this->_curl( $querydata );
		if ( "SUCCESS" == strtoupper( $_result["ACK"] ) || "SUCCESSWITHWARNING" == strtoupper( $_result["ACK"] ) )
		{
			$this->ack = $_result["ACK"];
			$this->token = urldecode( $_result["TOKEN"] );
			return $_result;
		}
		return false;
	}
	
	public function getDetails($token, $payerid = null)
	{
		if ( ! $token || 20 != strlen( $token ) ) die( 'token is null' );
		
		$querydata['METHOD'] = 'getExpressCheckoutDetails';
		$querydata['VERSION'] = '92.0';
		$querydata['TOKEN'] = $token;
		
		// Set the curl parameters.
		
		$_result = $this->_curl( $querydata );
		var_dump($_result);
		if ( "SUCCESS" == strtoupper( $_result["ACK"] ) || "SUCCESSWITHWARNING" == strtoupper( $_result["ACK"] ) )
		{
			$this->token = $token;
			$this->ack = $_result['ACK'];
			$this->checkoutstatus = $_result['CHECKOUTSTATUS'];
			$this->payerstatus = $_result['PAYERSTATUS'];
			$this->payerid = $_result['PAYERID'];
			$this->payemail = urldecode( $_result['EMAIL'] );
			$this->countrycode = urldecode( $_result['COUNTRYCODE'] );
			$this->currenycode = urldecode( $_result['CURRENCYCODE'] );
			$this->amt = urldecode( $_result['AMT'] );
			// PaymentActionNotInitiated PaymentActionFailer
			// PaymentActionInProgress PaymentActionCompleted
			
			if ( 'PaymentActionNotInitiated' == $this->checkoutstatus )
			{
				$this->paymentaction = 'Sale';
			}
			
			if ( $_result['BILLINGAGREEMENTACCEPTEDSTATUS'] && '1' == $_result['BILLINGAGREEMENTACCEPTEDSTATUS'] )
			{
				$this->billingagreementacceptedstatus = '1';
			}
			
			return $_result;
		}
		else
		{
			return false;
		}
	}
	
	public function doPayment(array $options = array())
	{
		if ( ! $this->token || 20 != strlen( $this->token ) ) return false;
		if ( ! $this->payerid || ! $this->amt || ! $this->paymentaction ) return false;
		
		$querydata['METHOD'] = 'doExpressCheckoutPayment';
		$querydata['VERSION'] = '92.0';
		$querydata['TOKEN'] = $this->token;
		$querydata['PAYMENTACTION'] = $this->paymentaction;
		$querydata['PAYERID'] = $this->payerid;
		$querydata['AMT'] = $this->amt;
		$_result = $this->_curl( $querydata );
		var_dump($_result);
		if ( "SUCCESS" == strtoupper( $_result["ACK"] ) || "SUCCESSWITHWARNING" == strtoupper( $_result["ACK"] ) )
		{
			$this->ack = $_result['ACK'];
			$this->paymentstatus = $_result['PAYMENTSTATUS'];
			$this->paymenttype = $_result['PAYMENTTYPE'];
			return $_result;
		}
		else
		{
			return false;
		}
	}
}