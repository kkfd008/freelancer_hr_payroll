<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_bonuses extends sp_controller
{
	public function index()
	{
		$option['domain']['url'] = sp_environment::get( 'domain:url' );
		$option['bonuses'] = array();
		$option['total'] = null;
		$bonuses = new model_bonuse( );
		$option['bonuses'] = $bonuses->link( )->fetch( );
		$template = 'default/bonuses.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		return true;
	}
	
	public function edit()
	{
		
		$ax_administrator_id = $this->request->get->aaid;
		
		$idx = $this->request->get->idx;
		if ( $idx )
		{
			$bonuse = new model_bonuse( );
			$result = $bonuse->link( )->filter( array(
				'idx=' . $idx
			) )->fetch( );
			if ( 1 == sizeof( $result ) )
			{
				$option['bonuses'] = $result[0];
			}
		}
		$option['domain']['url'] = sp_environment::get( 'domain:url' );
		$template = 'default/bonuses_add.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		return true;
	}
	
	public function save()
	{
		$data = $condition = array();
		$data = array(
			'ax_administrator_id' => $this->request->post->ax_administrator_id,
			'name' => $this->request->post->name,
			'datetime' => $this->request->post->datetime,
			'amount' => $this->request->post->amount,
			'currency' => $this->request->post->currency,
			'paymode' => $this->request->post->paymode,
			'scale' => $this->request->post->scale,
			'payer' => $this->request->post->payer,
			'memo' => $this->request->post->memo
		);
		$user = new model_bonuse( );
		if ( $this->request->post->idx )
		{
			$condition = array(
				'idx=' . $this->request->post->idx
			);
		}
		$result = $user->link( )->filter( $condition )->set( $data )->save( );
		
		$this->response->redirect = sp_environment::get( 'domain:url' ) . '?cmd=_bonuses_index';
		return true;
	}
	
	public function export()
	{
	
	}

}