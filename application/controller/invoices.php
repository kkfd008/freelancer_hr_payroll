<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_invoices extends sp_controller
{
	public function edit()
	{
		$option['domain']['url'] = sp_environment::get('domain:url');
		$option['invoices'] = true;
		$option['date'] = $date = $this->request->get->date;
		$option['aaid'] = $aaid = $this->request->get->aaid;
		$option['aaid'] = $aaid = 13;
		$option['cycle'] = $cycle = $this->request->get->cycle;
		if ( ! $date || ! $aaid || ! $cycle )
		{
			$option['invoices'] = false;
		}
		else
		{
			$staff = new model_staff( );
			$option['staff'] = $staff->link()->filter( array(
				'ax_administrator_id = ' . $aaid
			) )->fetch( );
			if ( 1 == sizeof( $option['staff'] ) && null != $option['staff'][0]['flexi_account'] && null != $option['staff'][0]['flexi_pass'] )
			{
				
				$maxday = date( 't', strtotime( $date ) );
				if ( 1 == $cycle )
				{
					$option['startdate'] = $date . '-01';
					$option['enddate'] = $date . '-15';
				}
				else
				{
					$option['startdate'] = $date . '-16';
					$option['enddate'] = $date . '-' . $maxday;
				}
			}
			
			$flexi = new helper_flexi( );
			$viewhours = $flexi->logon( $option['staff'][0]['flexi_account'], $option['staff'][0]['flexi_pass'] )->viewhours( $option['staff'][0]['flexi_account'], $option['startdate'], $option['enddate'] );
			$option['gener'] = $viewhours[sizeof( $viewhours ) - 1];
			// var_dump($option);
		
		}
		
		
		
		$template = 'default/invoices.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		
		$this->response->output->body = $context;
		return true;
	}
	
	public function save()
	{
		$data = $condition = array();
		$data = array(
			'ax_administrator_id' => $this->request->post->aaid,
			'date' => $this->request->post->date,
			'cycle' => $this->request->post->cycle,
			'hours' => $this->request->post->date_hours,
			'payroll' => $this->request->post->payroll,
			'allowance' => $this->request->post->allowance,
			'bonuses' => $this->request->post->bonuses,
			'due' => $this->request->post->amount_due,
			'sign' => $this->request->post->upload_file,
			
		);
		$user = new model_invoice();
		if ( $this->request->post->idx )
		{
			$condition = array(
				'idx=' . $this->request->post->idx
			);
			// $user->save( $data, $condition );
		}
		$result = $user->link()->filter($condition)->set($data)->save();
		$this->response->redirect = sp_environment::get('domain:url').'?cmd=_invoices_edit';
	}
	
	public function upload()
	{
		$error = "";
		$msg = "";
		$fileElementName = 'fileToUpload';
		if ( ! empty( $_FILES[$fileElementName]['error'] ) )
		{
			switch ( $_FILES[$fileElementName]['error'] )
			{
				
				case '1' :
					$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
					break;
				case '2' :
					$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
					break;
				case '3' :
					$error = 'The uploaded file was only partially uploaded';
					break;
				case '4' :
					$error = 'No file was uploaded.';
					break;
				
				case '6' :
					$error = 'Missing a temporary folder';
					break;
				case '7' :
					$error = 'Failed to write file to disk';
					break;
				case '8' :
					$error = 'File upload stopped by extension';
					break;
				case '999' :
				default :
					$error = 'No error code avaiable';
			}
		}
		elseif ( empty( $_FILES['fileToUpload']['tmp_name'] ) || $_FILES['fileToUpload']['tmp_name'] == 'none' )
		{
			$error = 'No file was uploaded..';
		}
		else
		{
			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize( $_FILES['fileToUpload']['tmp_name'] );
			
			$uploaddir = 'upload/';
			$uploadfile = $uploaddir . basename( $_FILES['fileToUpload']['name'] );
			move_uploaded_file( $_FILES['fileToUpload']['tmp_name'], $uploadfile );
			$msg = $uploadfile;
			
			// for security reason, we force to remove all uploaded file
			@unlink( $_FILES['fileToUpload'] );
		
		}
		echo json_encode( array(
			'error' => $error,
			'msg' => $msg
		) );
		die( );
	}
}