<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_staff extends sp_controller
{
	public function edit()
	{
		$redirect = false;
		$option['staff'] = array();
		$ax_administrator_id = $this->request->get->aaid;
		
		if ( $ax_administrator_id )
		{
			$staff = new model_staff( );
			$staffResult = $staff->link()->filter( array(
				'ax_administrator_id=' . $ax_administrator_id,
				'ax_administrator_id > 0'
			) )->fetch();
			if ( 1 == sizeof( $staffResult ) )
			{
				$mobile = explode( '-', $staffResult[0]['mobile'] );
				if ( 2 == sizeof( $mobile ) )
				{
					$staffResult[0]['mobile_area'] = $mobile[0];
					$staffResult[0]['mobile_number'] = $mobile[1];
				}
				else
				{
					$staffResult[0]['mobile_area'] = null;
					$staffResult[0]['mobile_number'] = null;
				}
				$option['staff'] = $staffResult[0];
			}
			else
			{
				$option['staff']['ax_administrator_id'] = $ax_administrator_id;
			}
		}
		else
		{
			$redirect = true;
		}
		
		if ( $redirect == true )
		{
			$this->response->redirect = sp_environment::get( 'domain:url' ) . '?cmd=_administrator_index';
		
		}
		else
		{
			$template = 'default/staff.html';
			$context = plugin_smarty::instance( )->fetch( $template, $option );
			$this->response->output->body = $context;
		}
		
		return true;
	}
	
	public function save()
	{
		$data = $condition = array();
		$data = array(
			'ax_administrator_id' => $this->request->post->ax_administrator_id,
			'name' => $this->request->post->name,
			'address' => $this->request->post->address,
			'country' => $this->request->post->country,
			'gmail' => $this->request->post->gmail,
			'mobile' => $this->request->post->mobile_area . '-' . $this->request->post->mobile_number,
			'skype' => $this->request->post->skype,
			'slaccount1' => $this->request->post->slaccount1,
			'slaccount2' => $this->request->post->slaccount2,
			'group' => $this->request->post->group,
			'amount' => $this->request->post->amount,
			'currency' => $this->request->post->currency,
			'allowance' => $this->request->post->allowance,
			'flexi_account' => $this->request->post->flexi_account,
			'flexi_pass' => $this->request->post->flexi_pass,
			'paypal_account' => $this->request->post->paypal_account
		);
		$user = new model_staff( );
		if ( $this->request->post->idx )
		{
			$condition = array(
				'idx=' . $this->request->post->idx
			);
			// $user->save( $data, $condition );
		}
		$user->link()->filter($condition)->set($data)->save();
		
		$this->response->redirect = sp_environment::get( 'domain:url' ) . '?cmd=_staff_edit&aaid=' . $this->request->post->ax_administrator_id;
		return true;
	}
}