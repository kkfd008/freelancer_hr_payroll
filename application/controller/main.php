<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_main extends sp_controller
{
	public function index()
	{
		// 获取用户资料
		// $staff_list = array(
		// '0' => array(
		// '_id' => '111111',
		// 'flexi_account' => 'anthony.acs8@gmail.com',
		// 'flexi_pass' => 'nbiezl'
		// )
		// );
		$staff = new model_staff( );
		$staff_list = $staff->link( )->fetch( );
		$flexi = new helper_flexi( );
		
		foreach ( $staff_list as $key => $value )
		{
			$flexi->logon( $value['flexi_account'], $value['flexi_pass'] );
			$date = $flexi->viewscreens( $value['flexi_account'], date( 'Y-m-d' ) );
			$staff_list[$key]['viewscreens'] = $date;
		}
		
		$option['staff_list'] = $staff_list;
		$option['domain']['url'] = sp_environment::get( 'domain.url' );
		$template = 'default/main.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		
		return true;
	
	}
	
	public function getpass($seluser = null)
	{
		$seluser = $this->request->get->seluser;
		$staff = new model_staff( );
		$data = $staff->link( )->filter( array(
			'flexi_account = "' . $seluser . '"'
		) )->fetch( );
		if ( 1 == sizeof( $data ) ) return $data[0]['flexi_pass'];
		
		return false;
	}
	
	public function getimg()
	{
		$seluser = $this->request->get->seluser;
		$date = $this->request->get->date;
		$time = $this->request->get->time;
		$flexi = new helper_flexi( );
		if ( false != $passwd = $this->getpass( $seluser ) )
		{
			$flexi->logon( $seluser, $passwd );
			$data = $flexi->getimg( $seluser, $date, $time );
			header( "Content-type: image/gif" );
			echo $data;
		}
		die( );
	}
	
	public function fullimg()
	{
		$seluser = $this->request->get->seluser;
		$date = $this->request->get->date;
		$time = $this->request->get->time;
		$flexi = new helper_flexi( );
		if ( false != $passwd = $this->getpass( $seluser ) )
		{
			// var_dump("$seluser, $passwd");
			$flexi->logon( $seluser, $passwd );
			$data = $flexi->fullimg( $seluser, $date, $time );
			header( "Content-type: image/gif" );
			echo $data;
		}
		die( );
	}
} 