<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_worktime extends sp_controller
{
	public function index()
	{
		$option['aaid'] = $this->request->get->aaid;
		$staff = new model_staff( );
		$option['staff'] = $staff->find( array(
			'ax_administrator_id = ' . $option['aaid']
		) );
		if ( 1 == sizeof( $option['staff'] ) && null != $option['staff'][0]['flexi_account'] && null != $option['staff'][0]['flexi_pass'] )
		{
			$option['startdate'] = $option['enddate'] = null;
			
			if ( 'search' == $this->request->post->submit )
			{
				$option['startdate'] = $this->request->post->startdate;
				$option['enddate'] = $this->request->post->enddate;
			}
			
			if ( null == $option['startdate'] ) $option['startdate'] = date( 'Y-m' ) . '-01';
			if ( null == $option['enddate'] ) $option['enddate'] = date( 'Y-m-d' );
		}
		
		$starttime = '';
		$endtime = '';
		$flexi = new helper_flexi( );
		$option['viewhours'] = $flexi->logon( $option['staff'][0]['flexi_account'], $option['staff'][0]['flexi_pass'] )->viewhours( $option['staff'][0]['flexi_account'], $option['startdate'], $option['enddate'] );
		// var_dump($option['viewhours']);
		
		$template = 'default/workTime.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		
		return true;
	}
	
	public function getimg()
	{
		$seluser = $this->request->get->seluser;
		$date = $this->request->get->date;
		$time = $this->request->get->time;
		$flexi = new helper_flexi( );
		$data = $flexi->logon( )->getimg( $seluser, $date, $time );
		header( 'Content-type: image/jpeg' );
		echo $data;
		die( );
	}
	
	public function fullimg()
	{
		$seluser = $this->request->get->seluser;
		$date = $this->request->get->date;
		$time = $this->request->get->time;
		$flexi = new helper_flexi( );
		$data = $flexi->logon( )->fullimg( $seluser, $date, $time );
		header( 'Content-type: image/jpeg' );
		echo $data;
		die( );
	}

}