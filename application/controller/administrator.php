<?php
define( 'FRAMEWORK_TEMPLATE_PATH', PROJECT_DOCUMENT_ROOT . '/theme' );
class controller_administrator extends sp_controller
{
	public function index()
	{
		$administrator = new model_administrator();
		$option['administrator'] = $administrator->link()->fetch();
		//var_dump($option);
		$template = 'default/administrator.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		return true;
	}
}