<?php
class controller_message extends sp_controller
{
	public function alert()
	{
		
		$option['msg'] = $this->request->get->msg;
		$template = 'default/message.html';
		$context = plugin_smarty::instance( )->fetch( $template, $option );
		$this->response->output->body = $context;
		return true;
		
	}
}