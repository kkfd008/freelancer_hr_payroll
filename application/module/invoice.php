<?php
class model_invoice extends sp_model
{
	protected $stroe = 'extends_db_mysql';
	protected $database = 'freelancer';
	protected $table = 'invoice';
	protected $field ='ax_administrator_id,idx,data,cycle,total_billable_hours,payroll,allowance,bonuses,total_amount_due,stats';
}